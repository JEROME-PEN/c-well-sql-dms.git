USE [master]
GO
/****** Object:  Database [dms]    Script Date: 11/24/2019 21:42:14 ******/
CREATE DATABASE [dms] ON  PRIMARY 
( NAME = N'dms', FILENAME = N'C:\Users\shinelon\Desktop\咸鱼\数据库大作业\发货内容\test.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'test_log', FILENAME = N'C:\Users\shinelon\Desktop\咸鱼\数据库大作业\发货内容\test.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [dms] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dms].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dms] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [dms] SET ANSI_NULLS OFF
GO
ALTER DATABASE [dms] SET ANSI_PADDING OFF
GO
ALTER DATABASE [dms] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [dms] SET ARITHABORT OFF
GO
ALTER DATABASE [dms] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [dms] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [dms] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [dms] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [dms] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [dms] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [dms] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [dms] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [dms] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [dms] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [dms] SET  DISABLE_BROKER
GO
ALTER DATABASE [dms] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [dms] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [dms] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [dms] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [dms] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [dms] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [dms] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [dms] SET  READ_WRITE
GO
ALTER DATABASE [dms] SET RECOVERY FULL
GO
ALTER DATABASE [dms] SET  MULTI_USER
GO
ALTER DATABASE [dms] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [dms] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'dms', N'ON'
GO
USE [dms]
GO
/****** Object:  Table [dbo].[Building]    Script Date: 11/24/2019 21:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Building](
	[bid] [char](4) NOT NULL,
	[num] [int] NOT NULL,
	[local] [nvarchar](50) NULL,
 CONSTRAINT [PK_Building] PRIMARY KEY CLUSTERED 
(
	[bid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Building] ([bid], [num], [local]) VALUES (N'c1  ', 200, N'商业街西，第一二食堂以东')
INSERT [dbo].[Building] ([bid], [num], [local]) VALUES (N'c10 ', 100, N'商业街西，第一二食堂以东')
INSERT [dbo].[Building] ([bid], [num], [local]) VALUES (N'c2  ', 100, N'c1以北')
INSERT [dbo].[Building] ([bid], [num], [local]) VALUES (N'c3  ', 100, N'c1以北')
INSERT [dbo].[Building] ([bid], [num], [local]) VALUES (N'c4  ', 100, N'商业街西，第一二食堂以东')
INSERT [dbo].[Building] ([bid], [num], [local]) VALUES (N'c5  ', 100, N'')
INSERT [dbo].[Building] ([bid], [num], [local]) VALUES (N'c6  ', 100, N'')
INSERT [dbo].[Building] ([bid], [num], [local]) VALUES (N'c7  ', 100, N'')
INSERT [dbo].[Building] ([bid], [num], [local]) VALUES (N'c8  ', 100, N'商业街西，第一二食堂以东')
/****** Object:  Table [dbo].[Visiter]    Script Date: 11/24/2019 21:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Visiter](
	[vid] [bigint] NOT NULL,
	[name] [nvarchar](10) NOT NULL,
	[phone] [nvarchar](20) NULL,
	[intime] [datetime] NOT NULL,
	[outtime] [datetime] NOT NULL,
 CONSTRAINT [PK_Visiter] PRIMARY KEY CLUSTERED 
(
	[vid] ASC,
	[intime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Visiter] ([vid], [name], [phone], [intime], [outtime]) VALUES (44050619880908, N'夏季赛', N'13513992285', CAST(0x0000A95500000000 AS DateTime), CAST(0x0000A95F00000000 AS DateTime))
INSERT [dbo].[Visiter] ([vid], [name], [phone], [intime], [outtime]) VALUES (44060618861028, N'周欧杰', N'1368292541', CAST(0x0000A8DC011082E4 AS DateTime), CAST(0x0000A8EB011082E4 AS DateTime))
INSERT [dbo].[Visiter] ([vid], [name], [phone], [intime], [outtime]) VALUES (440506188610281712, N' 周欧杰', N'13682972541', CAST(0x0000A67800000000 AS DateTime), CAST(0x0000A67900000000 AS DateTime))
/****** Object:  Table [dbo].[Student]    Script Date: 11/24/2019 21:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Student](
	[sid] [bigint] NOT NULL,
	[name] [varchar](10) NOT NULL,
	[sex] [varchar](2) NOT NULL,
	[birth] [datetime] NOT NULL,
	[faculty] [varchar](30) NOT NULL,
	[class] [varchar](30) NOT NULL,
	[phone] [bigint] NULL,
 CONSTRAINT [PK_student] PRIMARY KEY CLUSTERED 
(
	[sid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Student] ([sid], [name], [sex], [birth], [faculty], [class], [phone]) VALUES (20161008906, N'田秀雨', N'女', CAST(0x00008B9500000000 AS DateTime), N'土木学院', N'土木工程', 0)
INSERT [dbo].[Student] ([sid], [name], [sex], [birth], [faculty], [class], [phone]) VALUES (201610089066, N'陈莉莉', N'女', CAST(0x00008A8400000000 AS DateTime), N'管理学院', N'会计学1班', 0)
INSERT [dbo].[Student] ([sid], [name], [sex], [birth], [faculty], [class], [phone]) VALUES (201610089069, N'黄泽鹏', N'男', CAST(0x00008B9100000000 AS DateTime), N'计算机工程', N'计算机科学与技术', 88888888)
INSERT [dbo].[Student] ([sid], [name], [sex], [birth], [faculty], [class], [phone]) VALUES (201610089071, N'王尼玛', N'男', CAST(0x0000891700000000 AS DateTime), N'计算机工程', N'计算机科学与技术', 1234658)
INSERT [dbo].[Student] ([sid], [name], [sex], [birth], [faculty], [class], [phone]) VALUES (201610089072, N'曹尼玛', N'男', CAST(0x0000891700000000 AS DateTime), N'计算机工程', N'计算机科学与技术', 1234658)
INSERT [dbo].[Student] ([sid], [name], [sex], [birth], [faculty], [class], [phone]) VALUES (201610089073, N'穆银天', N'男', CAST(0x0000891700000000 AS DateTime), N'土木工程', N'建筑', NULL)
INSERT [dbo].[Student] ([sid], [name], [sex], [birth], [faculty], [class], [phone]) VALUES (201610089074, N'赵铁柱', N'男', CAST(0x0000891700000000 AS DateTime), N'计算机工程', N'计算机科学与技术', 1234658)
INSERT [dbo].[Student] ([sid], [name], [sex], [birth], [faculty], [class], [phone]) VALUES (201610089076, N'赵银柱', N'男', CAST(0x0000891700000000 AS DateTime), N'计算机工程', N'计算机科学与技术', 1234658)
INSERT [dbo].[Student] ([sid], [name], [sex], [birth], [faculty], [class], [phone]) VALUES (201610089077, N'赵石柱', N'男', CAST(0x0000891700000000 AS DateTime), N'计算机工程', N'计算机科学与技术', 1234658)
/****** Object:  Table [dbo].[Manager]    Script Date: 11/24/2019 21:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Manager](
	[mid] [int] NOT NULL,
	[pass] [char](10) NOT NULL,
	[name] [varchar](10) NOT NULL,
	[permision] [char](1) NOT NULL,
 CONSTRAINT [PK_manager] PRIMARY KEY CLUSTERED 
(
	[mid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Manager] ([mid], [pass], [name], [permision]) VALUES (160001, N'123456    ', N'黄泽鹏1', N'1')
INSERT [dbo].[Manager] ([mid], [pass], [name], [permision]) VALUES (160002, N'123456    ', N'黄泽鹏2', N'2')
/****** Object:  Table [dbo].[InOut]    Script Date: 11/24/2019 21:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InOut](
	[sid] [bigint] NOT NULL,
	[intime] [datetime] NOT NULL,
	[outtime] [datetime] NOT NULL,
	[over] [nvarchar](2) NOT NULL,
	[reason] [nvarchar](50) NULL,
 CONSTRAINT [PK_InOut] PRIMARY KEY CLUSTERED 
(
	[sid] ASC,
	[intime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[InOut] ([sid], [intime], [outtime], [over], [reason]) VALUES (201610089073, CAST(0x0000A3D100C65BB0 AS DateTime), CAST(0x0000A53F00000000 AS DateTime), N'否', N' ')
INSERT [dbo].[InOut] ([sid], [intime], [outtime], [over], [reason]) VALUES (201610089073, CAST(0x0000A3D200000000 AS DateTime), CAST(0x0000A53F00000000 AS DateTime), N'否', N'')
INSERT [dbo].[InOut] ([sid], [intime], [outtime], [over], [reason]) VALUES (201610089073, CAST(0x0000A3D200C65BB0 AS DateTime), CAST(0x0000A53F00000000 AS DateTime), N'否', N' ')
INSERT [dbo].[InOut] ([sid], [intime], [outtime], [over], [reason]) VALUES (201610089073, CAST(0x0000A77E00F27B7E AS DateTime), CAST(0x0000A77E00F27B7E AS DateTime), N'否', N'')
INSERT [dbo].[InOut] ([sid], [intime], [outtime], [over], [reason]) VALUES (201610089073, CAST(0x0000A8D700000000 AS DateTime), CAST(0x0000A8E600000000 AS DateTime), N'否', N'')
INSERT [dbo].[InOut] ([sid], [intime], [outtime], [over], [reason]) VALUES (201610089073, CAST(0x0000A8D800000000 AS DateTime), CAST(0x0000A8E600000000 AS DateTime), N'否', N'')
INSERT [dbo].[InOut] ([sid], [intime], [outtime], [over], [reason]) VALUES (201610089073, CAST(0x0000A8DA00000000 AS DateTime), CAST(0x0000A8E600000000 AS DateTime), N'否', N'')
INSERT [dbo].[InOut] ([sid], [intime], [outtime], [over], [reason]) VALUES (201610089073, CAST(0x0000A8E300F77C40 AS DateTime), CAST(0x0000A77E00F27B7E AS DateTime), N'是', N'')
INSERT [dbo].[InOut] ([sid], [intime], [outtime], [over], [reason]) VALUES (201610089073, CAST(0x0000A8E400F77C40 AS DateTime), CAST(0x0000A77E00F27B7E AS DateTime), N'是', N'')
/****** Object:  Table [dbo].[Dormitory]    Script Date: 11/24/2019 21:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dormitory](
	[did] [int] NOT NULL,
	[bid] [char](4) NOT NULL,
	[dnum] [int] NOT NULL,
	[floor] [int] NOT NULL,
	[bed] [int] NOT NULL,
	[price] [float] NOT NULL,
 CONSTRAINT [PK_Dormitory] PRIMARY KEY CLUSTERED 
(
	[did] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Dormitory] ([did], [bid], [dnum], [floor], [bed], [price]) VALUES (1, N'c1  ', 106, 1, 1, 1500)
INSERT [dbo].[Dormitory] ([did], [bid], [dnum], [floor], [bed], [price]) VALUES (2, N'c1  ', 107, 1, 2, 1500)
INSERT [dbo].[Dormitory] ([did], [bid], [dnum], [floor], [bed], [price]) VALUES (3, N'c2  ', 106, 1, 4, 1500)
INSERT [dbo].[Dormitory] ([did], [bid], [dnum], [floor], [bed], [price]) VALUES (4, N'c2  ', 107, 1, 4, 1500)
INSERT [dbo].[Dormitory] ([did], [bid], [dnum], [floor], [bed], [price]) VALUES (5, N'c3  ', 107, 1, 4, 1500)
INSERT [dbo].[Dormitory] ([did], [bid], [dnum], [floor], [bed], [price]) VALUES (6, N'c3  ', 108, 1, 4, 1500)
/****** Object:  Table [dbo].[StuDormitory]    Script Date: 11/24/2019 21:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StuDormitory](
	[did] [int] NOT NULL,
	[sid] [bigint] NOT NULL,
 CONSTRAINT [PK_StuDormitory] PRIMARY KEY CLUSTERED 
(
	[did] ASC,
	[sid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[StuDormitory] ([did], [sid]) VALUES (1, 201610089076)
INSERT [dbo].[StuDormitory] ([did], [sid]) VALUES (1, 201610089077)
INSERT [dbo].[StuDormitory] ([did], [sid]) VALUES (2, 20161008906)
INSERT [dbo].[StuDormitory] ([did], [sid]) VALUES (2, 201610089066)
INSERT [dbo].[StuDormitory] ([did], [sid]) VALUES (3, 201610089074)
INSERT [dbo].[StuDormitory] ([did], [sid]) VALUES (4, 201610089069)
INSERT [dbo].[StuDormitory] ([did], [sid]) VALUES (4, 201610089071)
INSERT [dbo].[StuDormitory] ([did], [sid]) VALUES (4, 201610089073)
/****** Object:  Table [dbo].[Adjust]    Script Date: 11/24/2019 21:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adjust](
	[sid] [bigint] NOT NULL,
	[adate] [date] NOT NULL,
	[olddid] [int] NOT NULL,
	[newdid] [int] NOT NULL,
	[reason] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid] ASC,
	[adate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Adjust] ([sid], [adate], [olddid], [newdid], [reason]) VALUES (201610089066, CAST(0x403E0B00 AS Date), 6, 2, N'')
INSERT [dbo].[Adjust] ([sid], [adate], [olddid], [newdid], [reason]) VALUES (201610089073, CAST(0x353E0B00 AS Date), 3, 4, N'')
INSERT [dbo].[Adjust] ([sid], [adate], [olddid], [newdid], [reason]) VALUES (201610089073, CAST(0x403E0B00 AS Date), 2, 3, N'')
/****** Object:  ForeignKey [FK_InOut_Student]    Script Date: 11/24/2019 21:42:15 ******/
ALTER TABLE [dbo].[InOut]  WITH CHECK ADD  CONSTRAINT [FK_InOut_Student] FOREIGN KEY([sid])
REFERENCES [dbo].[Student] ([sid])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[InOut] CHECK CONSTRAINT [FK_InOut_Student]
GO
/****** Object:  ForeignKey [FK_Dormitory_Building]    Script Date: 11/24/2019 21:42:15 ******/
ALTER TABLE [dbo].[Dormitory]  WITH CHECK ADD  CONSTRAINT [FK_Dormitory_Building] FOREIGN KEY([bid])
REFERENCES [dbo].[Building] ([bid])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Dormitory] CHECK CONSTRAINT [FK_Dormitory_Building]
GO
/****** Object:  ForeignKey [FK__StuDormitor__sid__0CBAE877]    Script Date: 11/24/2019 21:42:15 ******/
ALTER TABLE [dbo].[StuDormitory]  WITH CHECK ADD  CONSTRAINT [FK__StuDormitor__sid__0CBAE877] FOREIGN KEY([sid])
REFERENCES [dbo].[Student] ([sid])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StuDormitory] CHECK CONSTRAINT [FK__StuDormitor__sid__0CBAE877]
GO
/****** Object:  ForeignKey [FK_StuDormitory_Dormitory]    Script Date: 11/24/2019 21:42:15 ******/
ALTER TABLE [dbo].[StuDormitory]  WITH CHECK ADD  CONSTRAINT [FK_StuDormitory_Dormitory] FOREIGN KEY([did])
REFERENCES [dbo].[Dormitory] ([did])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[StuDormitory] CHECK CONSTRAINT [FK_StuDormitory_Dormitory]
GO
/****** Object:  ForeignKey [FK__Adjust__olddid__656C112C]    Script Date: 11/24/2019 21:42:15 ******/
ALTER TABLE [dbo].[Adjust]  WITH CHECK ADD  CONSTRAINT [FK__Adjust__olddid__656C112C] FOREIGN KEY([olddid])
REFERENCES [dbo].[Dormitory] ([did])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Adjust] CHECK CONSTRAINT [FK__Adjust__olddid__656C112C]
GO
/****** Object:  ForeignKey [FK__Adjust__sid__6477ECF3]    Script Date: 11/24/2019 21:42:15 ******/
ALTER TABLE [dbo].[Adjust]  WITH CHECK ADD  CONSTRAINT [FK__Adjust__sid__6477ECF3] FOREIGN KEY([sid])
REFERENCES [dbo].[Student] ([sid])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Adjust] CHECK CONSTRAINT [FK__Adjust__sid__6477ECF3]
GO
